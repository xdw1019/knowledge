# 如何提交一个PR
   如果你熟悉git原理并在gitee上提交过代码，那么你基本可以忽略本章节内容，但是有两点务必注意：
+ **在你向OpenHarmony社区提交代码之前，务必先[签署DCO](#a-id签署dco签署dcoa)**
+ **任何的本地提交务必增加签名，git commit务必携带-s参数**
## GIT的基本原理
关于git的历史和原理，笔者找了一个介绍比较好的文章，大家可以参考：[https://zhuanlan.zhihu.com/p/66506485](https://zhuanlan.zhihu.com/p/66506485)
对于初学者而言，个人觉得使用下面的图来理解更合适。
![](./media/git基本流程.png)
1. fork：指的是从官网仓库中复制一份拷贝到自己的账号仓库下，在这个时间节点下两者的内容一致；后续需要不断的手动完成同步；
2. clone:指的是从自己的账号仓库下下载到本地端;
3. commit:指的是将克隆的代码，根据需要修改更正某些内容或者增加新内容、删除冗余内容，形成记录。
4. push:指的是将自己的修改提交到本人账号仓库下；
5. pr:指的是将自己的修改从自己的账号仓库下提交到官方账号仓库下；
6. merge:指的是官方账号仓库的commiter接受了你的修改；
7. fetch:指的是将官方账号仓库的内容拉取到本地。

几个问题：
+ 可以直接从官方账号仓库clone吗？
  当然可以的，只不过一般而言我们很难直接获取向官方账号push的权限，所以建议先fork然后从自己账号仓库下clone.
+ 如何保持自己的账号仓库和官方账号仓库同步？
  可以直接使用网页上的同步功能；另外一种就是示意图上的先从官方仓库fetch一份，然后推送到本人账号仓库即可。
+ clone和fork是每次都必须的？
  fork可以理解为一次服务器端的初始化拷贝，只需一次即可；clone是从服务器到本地的一次初始化拷贝，只需要一次即可。

## PR实操
### 基本工具和配置环境
#### GIT工具
WINDOWS环境下建议大家使用命令行的工具，如果你是MACOS或者Linux,我相信你使用起来会更简单，此处不表，本文仅仅以WINDOWS环境介绍。
可以从[git bash下载地址](https://git-scm.com/download/win)下载git bash工具并安装。
安装完毕之后，在你的工作目录下右键点击即可出现git bash。

&nbsp;![](./media/gitbash启动.png)

点击启动git bash之后会进入一个linux终端的界面，这就是我们后续将修改内容从本地上传到

Gitee上的个人仓库的主要战场了。

&nbsp;![](./media/gitbash界面.png)

一些linux的基本命令（cd：切换目录；cat:参看文件等）都可以在这个界面使用，使用help + 命令可以查看具体的命令的使用方法。请记住，你可以输入命令的前几个字符然后使用tab键补全，毕竟大家的记忆都不是很好。

&nbsp;![](./media/linux命令查看.png)

#### 补充SSH公钥
使用SSH公钥可以让你在你的电脑和 Gitee 通讯的时候使用安全连接。
那么怎么获取到我们PC的SSH公钥呢？在桌面右键打开git bash

输入**ssh-keygen.exe** 并回车，再次回车，然后输入y，继续回车两次，这样即可生成个人的SSH公钥保持文件。

![](./media/sshkeygen.png)

git无法直接ctrl+c/v实现复制粘贴，但可以鼠标选中ssh公钥保持文件（即Your public key has been saved in 后面的内容）然后右键Copy复制，Paste粘贴实现这个功能。

使用cat命令查看生成的id_rsa.pub文件，输入cat （右键Paste粘贴ssh公钥保持文件）回车即可查看具体信息。

![](./media/id_rsapub.png)
从ssh-rsa开始，整段选中然后复制，打开gitee官网在设置里面找到ssh公钥，粘贴确定即可将公钥添加到我们的gitee账号中。
![](./media/ssh公钥设置.png)
![](./media/添加完毕SSH公钥.png)

#### 配置提交信息
我们向gitee个人仓库提交修改内容，需要告知大家这些修改内容是谁发起提交的，不然大家怎么知道是哪位英雄好汉为开源社区出了力。所以为了避免每次都重复输入一些提交信息（个人账号信息），我们需要使用git bash统一配置一下提交信息。

首先，先记住自己的个人空间地址，在个人主页的网页链接上可看到。

![](./media/个人空间地址.png)

打开git bash，依次输入以下命令并回车，前两个命令没有反应就证明配置成功。

```
git config --global user.name "xxxx"   （配置用户名，xxxx为账号用户名，即个人空间地址）
git config --global user.email "xxxxxx@xxx"      
git config --list         （查看配置情况）
```


![](./media/提交信息配置.png)

**恭喜你，到这里就顺利完成向开源社区做贡献的准备动作了！**

#### 安装代码以及文档编辑IDE
如果你没有个人熟悉的工具，个人推荐使用OpenHarmony南向设备端IDE DevEcoTool作为我们的C/C++/Markdown文档的编辑工具，不然很多MD文档（程序员最爱）你都打不开。
[IDE下载地址：](https://device.harmonyos.com/)
![](./media/IDE下载界面.png)
下载对应的版本之后按照安装引导进行安装。
![](./media/IDE安装.png)

这是一款给予Visual Studio Code的IDE，可以安装很多VS插件，比方我们修改文档常用的Markdown.打开该IDE工具，在扩展插件中搜索Markdown，安装Markdown All in one/Markdown preview enchance/Markdown PDF几个插件。

![](./media/markdown插件.png)

### 让我们实操一下，进入PR对话之旅
**目标：知识体系联合运营小组、基础设施组的同学举办了一个知识赋能课程直播，直播活动有个课后打卡的环节，我们就来提个PR做个打卡的动作吧。**

#### Fork官方仓库

把官方仓库内容同步到自己的个人仓库

[活动仓库的官方地址](https://gitee.com/openharmony-sig/online_event)
![](./media/fork官方仓库.png)

fork之后，在我们的gitee账号就可以看到这个仓库啦。

![](./media/fork到个人账号.png)
#### Clone仓库内容到本地
打开到个人账号下面的这个仓库，进入到clone界面，复制clone的链接地址。

![](./media/进入到clone界面.png)

在gitbash工具下面使用git clone命令完成clone动作。--depth=1意思是只clone当前仓库最新版本，避免仓库历史记录过于庞大花费太多clone时间。

```
git clone https://gitee.com/Cruise2022/online_event.git --depth=1
```
![](./media/clone过程.png)

clone完毕之后，即可在本地目录下看到这个clone的仓库。补充说明一下，本地目录所在位置是根据git bash的位置决定的，比如你在桌面启动git bash，则clone的仓库会出现在桌面。

![](./media/clone完毕.png)

#### 编辑本地文件
在本地目录打开六期成长打卡作业，新建一个打卡作业文件夹（推荐使用自己的gitee个人空间地址来命名自己的打卡作业文件夹，避免冲突），把readme.md文档复制到自己的打卡作业文件夹内，可以使用刚才安装的IDE编辑md文件，也可以使用其他的markdown工具编辑这个md文件（例如Typora）
![](./media/gitee打卡内容.png)
#### 提交修改到本地仓库
readme.md修改完成注意保存后，然后开始使用git命令查看并提交。此时需要在本地仓库位置进行git bash，不然git找不到我们修改后的内容。提交的主要步骤如下：

1. 查看修改变更后的文件，输入以下命令查看
```
git  status
```
 ![](./media/查看变更文件.png)
2. 将修改变更后的文件添加到暂存区，输入以下命令操作
```
git  add *
```
![](./media/添加文件到暂存区.png)
3. 提交修改变更后的文件到本地仓库并签名，输入以下操作命令。
  -s是签名表明这次提交者是你，-m是对此次提交行为进行备注，向大家说明你提交修改了什么。
```
git commit -s -m  "add:知识体系赋能课程签到"
```
![](./media/提交修改到本地仓库并签名.png)
4. 最后我们再用git status查看一下，可看到已没有修改变更的东西存在了。
  ![](./media/无修改内容.png)
#### 推送本地仓库的新内容到账号仓库
1. 完成本地修改提交之后，这个修改内容会保存到本地仓库，并且形成了LOG。LOG是提交记录的意思，所有人在这个仓库内的操作都会形成LOG存储起来，方便随便回退修改。使用git log命令可以查看到我们此次的提交记录。（输入git log命令后无法输入其他命令，同时按ctrl+c就能退出来了。）
```
git log
```
![](./media/提交日志.png)
2. 现在我们需要将本地仓库的修改内容推送到gitee上的个人仓库，使用git push命令来完成这个动作。
```
git push origin master
```
![](./media/推送成功.png)
3. 进入我们的账号下面，我们查看这个仓库，发现已经发生了变化。

![](./media/账号仓库发生变化.png)

#### 从个人账号仓库下到官方仓库下提交PR
进入个人账号的该仓库下，点击增加PR即可开始提交PR。
![](./media/提交PR入口.png)
![](./media/书写PR内容.png)
提交之后就可以看到我们提交的PR了。
![](./media/提交的PR.png)

**此时已经结束了吗？**
No.我们需要在该PR下增加一个评论，门禁才开始检查（门禁指管理员设置的一系列合规性检查，检测代码是否合规，质量是否合格，是否签署DCO协议）。
![](./media/PR评论.png)
然后我们就可以看到CI的各种合规检查开始进行了，泡杯咖啡，坐等检查通过，然后仓库的管理员来进行合并（PS：有些提交会因为各种原因被管理员拒绝，注意留意管理员回复及时修改重新提交）

![](./media/PR检查.png)

**至此，恭喜你成功向OpenHarmony开源社区发出了第一声属于自己的呐喊！**

**虽然这只是一小步，但我们相信这会是社区进步的一大步，期待你加入我们，一起开源贡献！**

## FAQ

+ **我以前注册过gitee，本次使用新账号，但是push的时候失败**
  现象如下：
  ![](./media/推送失败.png)
  具体原因是我们的windows的SSH凭据可能还是保持的以前的设置，需要我们手动进行更改凭据条**git:https://gitee.com**。
  ![](./media/推送失败原因.png)

+ **visual studio code 安装完markdown相关的插件之后，没有预览功能**
  查看visual studio code是否处于Restricted Mode模式，看IDE界面的左下角，如果是，则点击关闭即可。在markdown界面右键即可出现预览功能。
  ![](./media/预览功能.png)

## 作业
+ 完成知识体系相关仓库社区交流（watch/fork/star）
+ 结合自己的视角，提出问题（ISSUE）并力所能及的修正问题（PR）可以检视下述仓库

  - [知识体系首页仓库](https://gitee.com/openhar-mony-sig/knowledge)
  - [知识体系样例仓库-智能家居场景](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
  - [知识体系样例仓库-影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
  - [知识体系样例仓库-购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)
  - [知识体系样例仓库-运动健康场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
  - [知识体系样例仓库-智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
  - [知识体系样例仓库-智慧办公场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
  - [知识体系样例仓库-其他场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
  - [知识体系活动仓库](https://gitee.com/openharmony-sig/online_event)


**[回到首页](../README_zh.md)**