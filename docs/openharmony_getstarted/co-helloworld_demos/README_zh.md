# 如何贡献一个hello_world 样例

## 前言

丰富多样的OpenHarmony开发样例离不开广大合作伙伴和开发者的贡献，知识体系项目组针对于[智能家居](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)、[智能出行](https://gitee.com/openharmony-sig/knowledge_demo_travel)、[购物消费](https://gitee.com/openharmony-sig/knowledge_demo_shopping)、[影音娱乐](https://gitee.com/openharmony-sig/knowledge_demo_entainment)等场景共建了很多开发样例。相关仓库中不仅有让你耳目一新的FA应用开发样例，还有针对于OpenHarmony轻量系统、小型系统上的设备开发样例。

欢迎大家来一起参与知识体系样例共建中来，如果如果你也想把自己开发的样例分享出来，欢迎把样例提交到OpenHarmony-SIG仓来。如果你还没有提交过相关样例，担心步骤复杂、踩坑多，在本篇文章中我以一个OpenHarmony 轻量系统Hi3861上的设备开发hello_world 样例为例。教你手把手开发、提交你自己的样例，你就是下一个大牛~~~ 。

&nbsp;<img src="./media/OpenHarmony.png" style="zoom:67%;" />

对于初学者来说，整个OpenHarmony相关源码庞大复杂，我们可以将其分成四大块部分，第一部分是device目录下的相关芯片soc代码；第二部分是 OpenHarmony相关系统特性能力（例如：分布式、ArkUI、 多媒体能力等等）； 第三部分是OpenHarmony 系统依赖的相关三方库能力；第四部分就是vendor目录下存放的个人和相关共建公司的定制化信息。

vendor目录下的相关定制化信息，不仅决定了你的这个样例需要依赖哪一个soc代码，而且还决定了我们本次样例涉及了哪些OpenHarmony系统特性和三方库文件。现在你知道vendor仓库下内容的重要性了吧？ 

对于我们知识体系中相关设备案例，我们在vendor 目录下新增了team_x选项，可以在前面相关场景仓的dev/team_x目录下面学习你刚兴趣的开发样例。



好吧，说了这么多，接下来我会从如下两个问题出发给大家具体讲述：

1）怎么开发一个样例，样例开发中有哪些详细步骤？  

2）样例开发出来后，我们怎样向知识体系仓库提交样例？

## 样例开发

关于如何开发一个样例，无论这个样例是FA应用样例还是设备侧开发样例，都有如下几个步骤和流程：

&nbsp;![](./media/demo_steps.jpg)

第一部分：开发环境准备，需要准备相关的开发环境， 例如FA应用开发，你也必须首先准备好开发板、DevEco Studio、下载相关的 

​                   OpenHarmony sdk。对于设备侧样例开发也是一样，你需要准备开发板、软件开发环境、准备好相关的OpenHarmony源              

​                   码；

第二部分：样例代码开发， 与FA应用开发类比，设备端也要需要新建样例工程，新增我们的样例产品，随后参照编码规范完成我们的样  

​                   例功能，还需要考虑相关样例符合开源的License和Copyright 要求；

第三部分：烧录运行，样例开发完成后实际烧录验证，并做相关优化；

第四部分：样例文档输出，样例文档是你的样例的第一入口，文档写的好能让其他同学快速的了解你的样例要点和关键特性。

### 开发环境准备

#### 硬件准备

润和Hispark_pegasus开发套件一套

#### 软件开发环境

关于OpenHarmony的开发环境准备，选择一个你最喜欢的方式即可。比如我习惯在Windows Visual Studio Code上阅读代码，然后利用DevEco Device Tool 工具进行代码编译。

![](./media/deveco_device_tool.png)

但是在当前阶段，大部分的开发板源码还不支持在Windows环境下进行编译，如Hi3861、Hi3516系列开发板。因此，需要使用Ubuntu的编译环境对源码进行编译。具体怎么样搭建Windows+Ubuntu混合开发环境，请参考 [混合开发环境搭建文档](https://device.harmonyos.com/cn/docs/documentation/guide/ide-install-windows-ubuntu-0000001194073744)

#### 源码下载

特别说明 ： 

接下来代码下载采用的是 使用DevEco Device Tool 工具下载 相关HPM 组件源码，如果你想利用repo 命令行的方式下载源码，请参考[README_zh_CMD_Version.md](./README_zh_CMD_Version.md)



1）首先进入"QUACK_ACCESS" -> "主页" 界面，点击“新建工程”选项； 

![](./media/new_project.png)

2）输入你的工程名名称，这里以hi3861_hello_world 为例，配置下载“@ohos/hispark_pegasus" 组件，其他选项默认即可，点击"创建按钮"；

(特别说明：因为我是用hi3861芯片 hispark_pegasus 开发板为例子，所以我选择了HPM 网站上自带的hispark_pegasus 组件，如果你对你所需要下载的相关组件不了解，请访问hpm组件网站: [DevEco Marketplace](https://hpm.harmonyos.com/#/cn/solution))

&nbsp;![](./media/projcet_download.png)



3、代码下载

&nbsp;![](./media/project_downloading.png)



### 样例代码开发

#### 新建样例工程

&nbsp;![](./media/team_x_hello_world.png)

在本章节中我将给大家讲述如何添加一个样例工程hi3861_hello_world 到OpenHarmony编译架构中去。现在我用如下步骤来说明：

1）添加product

product：产品就是你基于OpenHarmony系统希望实现哪些功能，在这里响当当的名字就叫 hi3861_hello_world，

 在上图中可以看到我把相关hello_world 样例的代码放到了vendor/team_x 目录

2）新增product 配置文件

在这里我基于OpenHarmony自带的Hispark_pegasus配置文件做了相关修改。首先删除了OpenHarmony自带的应用subsystem applications，然后将"product_name": "wifiiot_hispark_pegasus", 替换成为“hi3861_hello_world"。同样你在后面自己样例中，你可以以系统自带配置文件为基础，来开发定制你自己的样例工程。

3）将product 加入到编译体系中

在device/soc/hisilicon/hi3861v100/sdk_liteos/BUILD.gn 文件中做如下修改新增第26行

```
deps = [
 23     ":sdk",
 24     "//base/security/deviceauth/frameworks/deviceauth_lite:hichainsdk",
 25     "//build/lite:ohos",
 26     "//build/lite:product",  // 特别重要，添加此行
 27   ]
```



#### hello_world 样例编写

##### 样例内容详解

相关hello_world 样例已经上传到[相关链接](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/dev/team_x/hi3861_hello_world)，可以看到相关样例目录如下:

&nbsp;![](./media/vendor_dir.png)

在本章节我们重点解析demo_hello_world 目录中的内容，大家设想一下需要系统输出hello_world，需要哪些前提条件?

1）需要将包含打印语句函数编译到系统中去;

2）还需要确保能执行到该打印语句;

```
#gn 文件1：hi3861_hello_world/BUILD.gn
group("hi3861_hello_world") {  // group 中命名与product_name 保持一致
    deps = [
        "demo_hello_world:hello_world" // 将具体的hello_world 样例添加到轻量系统固件中去
    ]
}
```

如上图所示demo_hello_world 中的BUILD.gn文件就解决了第一个问题，我们将打印函数所在的源文件编译成一个静态库文件，然后在链接到Hi3861系统固件中，以下是BUILD.gn文件的详解，

```
# gn 文件2： hi3861_hello_world/demo_hello_world/BUILD.gn
static_library("hello_world") {  // 可以看到在这里的目的是要生成一个一个hello_world的静态库文件
    sources = [  // 静态库是由哪些源文件组成
        "main.c"  
    ]
}
```

main.c 文件详解

```
/**
 * @brief 你可以在该函数中撰写你自己的代码逻辑
 */
static void IotMainEntry(void)
{
    printf("Welcome to OpenHarmony, Hello World!!\n");
    return;
}
// APP_FEATURE_INIT 关键字将IotMainEntry 函数添加到系统启动流程中来
APP_FEATURE_INIT(IotMainEntry);
```

##### 遵循开发样例代码规范

+ 开发样例代码规范参考[OpenHarmony代码规范说明](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/贡献代码.md)

##### 源文件补充License和Copyright

首先我以一个OpenHarmony中的源码为例，给大家介绍什么是License和Copyright。

如下图所示，Copyright和License 都是放在一个文件的最开头，在源文件中用注释语法注释起来。

Copyright：版权声明，是代表作者的版权所有声明。下图的例子里面说明本文件是由华为公司2021年第一次发布的；

License：使用许可证， 是你授权其他用户合法使用软件的许可证。下图中说明了其他开发者可以在Apache2.0 的规则下面使用该文件。

&nbsp;![](./media/License_Copyright_example.png)

需要特别注意的是，不仅仅是.h/.c /.cpp 文件需要添加，我们在开发中涉及到的 *.java / *.ts / *.gn文件同样需要加上版权和许可证。

在实际代码开发中，请注意如下几个原则：

  （1）如果你使用了已发布的他人的相关开源代码，请保留原有的Copyright 和License；

  （2）如果相关文件是你的原创，请声明个人的版权说明 和License，License建议和OpenHarmony相关代码保持一致使用Apache2.0即可；

  （3）如果你使用了已发布的他人的相关开源代码，但是你基于原有代码有改动，那么你可以加上你自己的版权说明即可，特别注意需要保留原有的 Copyright和License。

 大家在在声明个人的License和Copyright文件时，可以参考[License&Copyright示例文件](./media/License&Copyright.c)

### 烧录运行

如果你之前没有相关DevEco device tool IDE的编译烧录经历，请访问如下文档：[编译Hi3861V100开发板源码](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861v100-compile-0000001192526021)

#### 编译关键步骤

1）修改本工程中设置的默认编译选项，将wifiiot_hispark_pegasus替换成为hi3861_hello_world；

&nbsp;![](./media/dist_dir.png)

/home/***/Documents/DevEco/Projects/hi3861_hello_world/ohos_bundles/@ohos/hispark_pegasus/scripts/dist.sh

```
#!/bin/bash
cd $DEP_BUNDLE_BASE
if [[ $xts ]]; then
	./test/xts/tools/lite/build.sh product=wifiiot xts=acts
elif [[ $incrementally ]]; then
	#python3 build.py -p wifiiot_hispark_pegasus@hisilicon  // 屏蔽本行
	python3 build.py -p hi3861_hello_world // 新增本行
else
	#python3 build.py -p wifiiot_hispark_pegasus@hisilicon -f  // 屏蔽本行
	python3 build.py -p hi3861_hello_world // 新增本行
fi
```
2） 点击编译图标编译；
![](./media/build_step.png)

点击"build"图标即可完成编译动作，待终端提示"hi3861_hello_world build success" 则表示相关固件。

#### 烧录关键步骤

1）设置固件路径；

![](./media/flash_config.png)

2）点击上图中所示Upload 图标，完成烧录动作；

### 样例文档

俗话说的好， 酒香也怕巷子深，如何让你的样例在在知识体系的众多样例中脱颖而出，甚至是去到OpenHarmony官网展示呢？样例相关文档就是你的关键所在了。大家可以参考[REDME 文档模板](../../co-construct_demos/git_readme_template.md)，规范完善你的样例文档。



## 样例提交

#### 选择提交的目的仓库

| 样例场景     | Openharmony-SIG样例仓库                                      |
| ------------ | ------------------------------------------------------------ |
| 智能家居场景 | [knowledge_demo_smart_home](https://gitee.com/openharmony-sig/knowledge_demo_smart_home) |
| 影音娱乐场景 | [knowledge_demo_entainment](https://gitee.com/openharmony-sig/knowledge_demo_entainment) |
| 购物消费场景 | [knowledge_demo_shopping](https://gitee.com/openharmony-sig/knowledge_demo_shopping) |
| 运动健康场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 智能出行场景 | [knowledge_demo_travel](https://gitee.com/openharmony-sig/knowledge_demo_travel) |
| 智慧办公场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 快速上手场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 其他场景     | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 赛事活动作品 | [online_event](https://gitee.com/openharmony-sig/online_event) |

```
knowledge_demo_smart_home/   // 开发样例场景仓库根目录
├── dev   // 用于存储轻/小型设备应用开发样例（C/C++）
│   ├── device  // 用于存储轻/小型设备的设备代码，仅仅托管不在开发板SIG/主干的开发板
│   │   ├── device_x1x
│   ├── team_x  // 样例目录，每个轻/小型开发样例都可以认为是一个产品
│   │   ├── demo_x1x  // 开发样例demo_x1x， 本开发样例相关的代码都放在该目录下
│   │   │   └── demo_x1x_main.c
│   │   └── demo_x2x
│   │       └── demo_x1x_main.c
│   └── third_party  // 轻/小型设备开发样例涉及的不在SIG/主干的三方库（组件）
│       ├── third_component_1  // 三方组件1
│       └── third_component_2
├── docs   // 所有的开发样例的说明文档目录
│   ├── demo_x1x_usage  // 开发样例demo_x1x的说明文档
│   │   ├── media   // 开发样例说明文档涉及到的相关媒体文件（图片动图等）目录
│   │   │   └── architecture.png  
│   │   └── README_zh.md  // 开发样例demo_x1x的主要说明文档
│   └── demo_x2x_usage
│       ├── media
│       │   └── architecture.png
│       └── README_zh.md
├── FA  // 标准设备开发样例工程目录
│   ├── FA_project_1  // 标准设备开发样例工程1
├── media // 开发样例场景仓库说明文档涉及的相关媒体文件目录
│   └── flow.png       
└── README_zh.md  // 开发样例场景仓库主说明文档
```



#### PR提交

到此为止，我已经准备好了我的hello_world样例，并且我也知道了如打算往知识体系的哪个OpenHarmony仓库提交。万里长征已经到了最关键的步骤，提交你的PR。关于如何提交一个PR，其中的详细步骤。我们可以访问[如何提交PR的指导文档](../push_pr/readme.md)



## FAQ

#### 

#### 1、PR提交后门禁提示dco检测失败，如何处理？

&nbsp;<img src="./media/check_dco_fail.png" style="zoom:67%;" />

如上图所示，openharmony_sig_ci 提示添加了"dco检查失败"的标签，请从如下两个方面检查：

1）未签署“DCO协议”，例如提示：

当前检测到如下commit的用户未签署DCO协议：

解决办法

点击[这里](https://gitee.com/link?target=https%3A%2F%2Fdco.openharmony.io%2Fsign%2FZ2l0ZWUlMkZvcGVuX2hhcm1vbnk%3D)签署、查看签署状态。

在PR的评论框输入check dco后，单击”评论”，系统将再次进行DCO校验。



2）Commits 中未包含 Signed-off-by信息，例如提示：

当前检测到如下commit未包含Signed-off-by信息

解决办法：

使用git commit 命令时，添加-s 选项；

在PR的评论框输入check dco后，单击”评论”，系统将再次进行DCO校验



#### 2、代码合规校验异常处理
&nbsp;![](./media/check_License_fail.png)

如上图所示，openharmony_sig_ci 提示添加了"代码合规检查失败"的标签，CI门禁会给出相关错误的具体原因，例如上图检测失败的原因就是其中上传了门禁不允许的MP4文件。

1）需要保证提交的文件有版权说明，并且不能篡改他人的版权头说明；

2）提交的文件需要有License，即许可说明；

3）传入非法格式文件，例如MP4或其他视频格式文件。

#### 3、 代码质量检测异常



&nbsp;<img src="./media/check_quality_fail.png" style="zoom:67%;" />

如上图所示，openharmony_sig_ci 提示添加了"代码质量检测失败"的标签，CI门禁会给出相关错误的具体原因。

常见代码质量问题有以下原因：

1、不要使用难以理解的常量（例如魔鬼数字）；
2、使用统一的大括号换行风格；
3、注释符与注释内容间要有1空格，tab 需要用四个空格对齐；
4、函数要简短，不要过大超过100行；

请参考OpenHarmony主仓中的[代码规范参考文章](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/贡献代码.md)

#### 4、MB级别的大文件如何提交

知识体系相关仓库中大文件需要使用git lfs 来管理，以免仓库过大不利于后期的维护和管理。接下来我们以在knowledge_demo_temp 仓库中提交一个jar包文件为例，演示一下具体步骤：

1）在相关仓库中准备好需要用git lfs 管理的文件，例如

knowledge_demo_temp/FA/IntelligentOffice/clockIn/gradle/wrapper/gradle-wrapper.jar 

2）具体操作指令

git lfs track FA/IntelligentOffice/clockIn/gradle/wrapper/gradle-wrapper.jar

git add .gitattributes // 使用track 命令后会在.gitattributes有相关提示

git add FA/IntelligentOffice/clockIn/gradle/wrapper/gradle-wrapper.jar

git lfs ls-files  // 查看此时仓库目录下的使用git lfs 管理的文件 
&nbsp;![img](./media/git_lfs_file.png)  

git commit -s -m "Upload the jar file by git lfs method"

git push origin master // 推送相关代码到你的个人仓库中

3）发起PR，将修改推送到OpenHarmony-sig 公共仓库

&nbsp;![img](./media/git_lfs_pr.png)                 

可以看到相关文件是以lfs 文件的形式存放到，OpenHarmony-sig 相关代码仓库中的，并没有占用sig 仓库大的空间。


## 参考资料

+ [开发样例场景仓库说明文档模板](../../co-construct_demos/git_readme_template.md)
+ [开发样例说明文档模板](../../co-construct_demos/demo_readme_template.md)
+ [OpenHarmony代码规范说明](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/贡献代码.md)
+ [开发样例代码提交说明](../../co-construct_demos/demo_code_submit.md)

