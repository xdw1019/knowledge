# 创建一个ISSUE
本节的主要内容是引导大家如何创建一个ISSUE,向社区提问，求助。
## 路见不平一声吼
  理论上，社区的迭代开发都是靠ISSUE推进的。笔者不才，引用其他人的解释：

issue中文可以翻译为‘事务’，指的是项目待完成的工作，在开源项目中应用很多，但在企业实际开发流程中，可能应用较少。issue在项目中可以承担用户反馈的作用，用户可以在这个地方提出bug反馈与优化建议，也可以为开发者服务，用于记录待完成的任务。每个issue可以包含该问题的前因后果，对于不了解项目的人员，整理的好的issue列表也有助于把握项目的优化内容。一般来说，可以包含以下类型：

1. 软件的bug
2. 功能优化建议
3. 待完成的任务


当你或者几个小伙伴在gitee创建仓库开始，后续就可以靠ISSUE推进迭代；提ISSUE的人可能是你熟悉的，也可能是一个路人。如果你觉得他的想法或者实现有提ISSUE的可能，那么你也可以提；但是你也应该理解，接受不接受是别人的事情。
比方我看到知识体系仓库中有个文档中有相关的链接没有附上。如下图：
![](./media/知识体系仓库问题.png)
遇到此问题，我们自然要发声了，开始提我们的第一个ISSUE之旅。
+ 找到本仓库的ISSUE入口
  ![](./media/ISSUE入口.png)
+ 提出问题：按照模板准确描述
  ![](./media/ISSUE.png)
+ 提交效果
  ![](./media/ISSUE情况.png)
## ISSUE的几点建议
+ gitee仓库是基于技术驱动的，请基于技术问题提出相关问题
+ 提ISSUE之前，可以在ISSUE清单中查看下是否已经有相关ISSUE进行中或者关闭
+ 在确定需要提ISSUE时，请按照模板具体描述相关问题；这个描述尽可能的描述清楚，让开发者很容易的get到你的需求或者问题所在
+ ISSUE一定要提给你的诉求仓库，否则可能被无视掉
## 小作业
感谢你来到OpenHarmony知识体系，看看这些仓库有什么问题？大家一起来找茬,比方链接是否正常？图片等链接文件的路径是否正确？相关说明文档的描述是否正确?
- [知识体系首页仓库](https://gitee.com/openharmony-sig/knowledge)
- [知识体系样例仓库-智能家居场景](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
- [知识体系样例仓库-影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
- [知识体系样例仓库-购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)
- [知识体系样例仓库-运动健康场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [知识体系样例仓库-智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
- [知识体系样例仓库-智慧办公场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [知识体系样例仓库-其他场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [OpenHarmony赛事活动作品](https://gitee.com/openharmony-sig/online_event)

## 思考题
我们为什么要使用git?git相对于我们传统的代码管理工具有什么优势？

**[回到首页](../README_zh.md)**